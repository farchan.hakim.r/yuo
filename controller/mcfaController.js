const include = require('./../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const Mcfa = require('./../models/mcfaModel');

const startDay = moment().startOf('day').format();
const endDay = moment().endOf('day').format();

function getAllMcfa(req,res,next) {
    if(res.statusCode == 200) {
        Mcfa.find({building_name : req.params.building_name, mcfa_type : req.params.mcfa_type}, function(err, mcfaFinds) {
                if(mcfaFinds.length < 1) {
                   res.status(404).json({
                       pesan : "data mcfa tidak ditemukan"
                   })
                   
                }else {
                    res.status(200).json({
                        pesan :  "data mcfa",
                        data_alert : mcfaFinds[0].detail,
                        data_zone  : mcfaFinds[0].detail_zone,
                        created_at : mcfaFinds[0].detail[0].created_at
                    })
                }
            });
     
    }else {
        res.status(404).json({
            pesan :"token tidak berlaku",        
        });
    }
}

function getMcfa(req,res,next) {
    let dataAlerts = [];
    if(res.statusCode == 200) {
        Mcfa.find({building_name : req.params.building_name, mcfa_type : req.params.mcfa_type}, function(err, mcfaFinds) {
                if(mcfaFinds.length < 1) {
                   res.status(404).json({
                       pesan : "data mcfa tidak ditemukan"
                   })
                   
                }else {
                    let lengthMcfa = mcfaFinds[0].detail[0].data.length;
                    let dataMcfa = mcfaFinds[0].detail[0].data;
                    let dataMcfa1 = mcfaFinds[0].detail[1].data;
                    let dataMcfa2 = mcfaFinds[0].detail[2].data
                    for(var i = 0; i < lengthMcfa; i++) {
                        if(dataMcfa[i] == false && dataMcfa1[i] == false && dataMcfa2[i] == false) {
                            dataAlerts.push('mcfa ' + mcfaFinds[0].detail_zone[i] + ' dalam bahaya');
                        }
                    }
                    res.status(200).json({
                        pesan :  "data mcfa",
                        data_alert : dataAlerts,
                        data_zone  : mcfaFinds[0].detail_zone,
                        last_alarm : mcfaFinds[0].last_time_alarm,
                        created_at : mcfaFinds[0].detail[0].created_at
                    })
                }
            });
     
    }else {
        res.status(404).json({
            pesan :"token tidak berlaku",        
        });
    }
}

function setMcfa(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.mcfa_type = req.params.mcfa_type;
	let mcfa = new Mcfa(data);
    let errData = mcfa.validateSync();
    
    if(res.statusCode == 200) {
        if(!errData) {
        //   if(data.detail_time_alarm) {
          //      data.last_time_alarm = moment().format();
           //}
            data.detail[0].created_at = moment().format();
            let mcfa = new Mcfa(data);
            Mcfa.find({building_name : req.params.building_name, pump_type : req.params.pump_type}, function(err, mcfaFinds) {
                if(mcfaFinds.length < 1) {
                    
                    mcfa.save(function (err, result) {
                        if (err) {
                            res.status(400).json({
                                pesan : "Gagal mengisi",
                            });			
                        } else {
                            res.status(200).json({
                                pesan : "Berhasil mengisi data",
                                data : data,
                            });
                        }
                    });
                }else {
                    Mcfa.findOneAndUpdate({building_name : req.params.building_name, mcfa_type : req.params.mcfa_type} , 
                        {   
                            $push : 
                            {
                                'detail' :
                                { 
                                    $each: data.detail,
                                    $position: 0,
                                    $slice: 1000
                                }
                            }
                        },
                        function(err,patient) {
                        res.status(200).json({
                            pesan :"tambah data ",
                            data : data
                        });
	//		console.log(JSON.stringify(data))
                    });
                }
            });
        } else {
            res.status(404).json({
                pesan :"data error",        
            });    
        }
    }else {
        res.status(404).json({
            pesan :"token tidak berlaku",        
        });
    }
    
}

function setZona(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.mcfa_type = req.params.mcfa_type;
	let mcfa = new Mcfa(data);
    let errData = mcfa.validateSync();

    if(res.statusCode = 200) {
        if(!errData) {
            Mcfa.find({building_name : req.params.building_name, pump_type : req.params.pump_type}, function(err, mcfaFinds) {
                if(mcfaFinds.length < 1) {
                    res.status(200).json({
                        pesan : "Data tidak tersedia",
                        data : data,
                    });
                }else {
                    Mcfa.findOneAndUpdate({building_name : req.params.building_name, mcfa_type : req.params.mcfa_type} , 
                        {   
                            $set : 
                            {
                                'detail_zone' : data.detail_zone
                            }
                        },
                        function(err,patient) {
                        res.status(200).json({
                            pesan :"menambah data zona",
                            data : data
                        });
                    });
                }
            });
        } else {
            res.status(404).json({
                pesan :"data error",        
            });    
        }
    }else {
        res.status(404).json({
            pesan :"token tidak berlaku",        
        });
    }
}

exports.setMcfa = setMcfa;
exports.getMcfa = getMcfa;
exports.getAllMcfa = getAllMcfa;
exports.setZona = setZona;
