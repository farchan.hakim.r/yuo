const include = require('./../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const Environment = require('./../models/kitchenModel');

const startDay = moment().startOf('day').format();
const endDay = moment().endOf('day').format();



function getEnvironment(req, res, next) {
    if (res.statusCode == 200) {
        Environment.find({id_jammah : req.params.id_jammah}, function(err, EnvironmentFinds) {
            if (EnvironmentFinds.length < 1) {
                res.status(404).json({
                    pesan: "data tidak ditemukan"
                })

            } else {
                let data = {
                    detail_data: EnvironmentFinds[0].detail_data
                }
                res.status(200).json({
                    pesan: "data sensor",
                    data: data
                })
            }
        });

    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }
}



function saveEnvironment(req, res, next) {
    let data = req.body;
    data.id_jammah = req.params.id_jammah;
    let environment = new Environment(data);
    let errData = environment.validateSync();
    data.detail_data[0].updated_at = moment().format();
    if (res.statusCode == 200) {
        if (!errData) {
            Environment.find({ id_jammah : req.params.id_jammah }, function(err, EnvironmentFinds) {
                if (EnvironmentFinds.length < 1) {
                    environment.save(function(err, result) {
                        if (err) {
                            res.status(400).json({
                                pesan: "Gagal mengisi",
                            });
                        } else {
                            res.status(200).json({
                                pesan: "Berhasil mengisi data",
                                data: data,
                            });
                        }
                    });
                } else {
                    Environment.findOneAndUpdate({id_jammah : req.params.id_jammah }, {
                            $push: {
                                'detail_data': {
                                    $each: data.detail_data,
                                    $position: 0,
                                    $slice: 1000
                                }

                            }
                        },
                        function(err, sukses) {
                            res.status(200).json({
                                pesan: "tambah data ",
                                data: data
                            });
                        });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

exports.saveEnvironment = saveEnvironment;
exports.getEnvironment = getEnvironment;
