const include = require('../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const Repair = require('../models/historyRepairModel');

const Staff = require('./../models/staffModel');
const startDay = moment().startOf('day').format();
const endDay = moment().endOf('day').format();


//KITA LIVE - /././././././././././././././../../../../../../../../../../>>?..>?>>>?>?<l:mn
//MDWEIOWENCIWNCLWNECLNWLNnlinldnlDLNdlNDLNLNMMDioipipipip nNLNLNlnllcdiNLNLLninnc3oinNN9NLNL
//NNOonoiooO8HOHOOnonono9H09HOO9j09jdcl99JJ0JNKJj9090kkPKED9E0lknlnl../../../../../..//
////.//.//..//..//.. 
function setRepair(req, res, next) {
    let data = req.body;
    let repair = new Repair(data);
    let errData = repair.validateSync();
    if (!errData) {
        Staff.find({ nip: data.nip }, function (err, StaffFinds) {
            if (StaffFinds.length < 1) {
                res.status(404).json({
                    pesan: "user tidak ditemukan"
                })
            } else {
                let id_staff = StaffFinds[0]._id;
                let id_device = req.params.id_device;
                let id_type =  req.params.type;
                data.id_type = id_type;
                data.id_staff = id_staff;
                data.id_device = id_device;
                
                data.id_building = req.params.building_name;
		        data.detail.created_at = moment().format();
                let RepairSave = new Repair(data);
		        console.log(data)
                RepairSave.save(function (err, result) {
                    if (err) {
                        res.status(400).json({
                            pesan: "Gagal mengisi",
                        });
                    } else {
                        Repair.updateMany({ id_staff: id_staff, id_device: id_device, id_type : id_type, id_building : data.id_building },
                            {
                                $set:
                                {
                                    'detail.status_repair': 1
                                }
                            },
                            function (err, result) {
                                res.status(200).json({
                                    pesan: "status di ubah ",
                                    status: 1,
                                    data: data
                                });
                            });

                    }

                });
            }
        });
    } else {
        res.status(404).json({
            errData
        })
    }
}

function changeStatusRepair(req, res, next) {
    let data = req.body;
    Staff.find({ nip: data.nip }, function (err, StaffFinds) {
        if (StaffFinds.length < 1) {
            res.status(404).json({
                pesan: "user tidak ditemukan"
            })
        } else {
            let id_staff = StaffFinds[0]._id;
            
            let id_device =req.params.id_device;
            let id_type =  req.params.type;
            data.id_type = id_type;
            data.id_staff = id_staff;
            data.id_device = id_device;
            data.id_building = req.params.building_name;
            let RepairSave = new Repair(data);
            Repair.find({ id_staff: id_staff, id_device: id_device , id_building : data.id_building, id_type : id_type}, function (err, RepairFind) {
                if (RepairFind.length < 1) {
                    res.status(404).json({
                        pesan: "data tidak ditemukan"
                    })
                } else {


                    Repair.updateMany({ id_staff: id_staff, id_device: id_device, id_building : data.id_building, id_type : id_type},
                        {
                            $set:
                            {
                                'detail.status_repair': data.status

                            }
                        },
                        function (err, result) {
                            res.status(200).json({
                                pesan: "status di ubah ",
                                status: data.status
                            });
                        });

                }
                 
            });
        }
    });
}

function cekRepairNow(req, res, next) {
    let data = req.body;
    
    Staff.find({ nip: data.nip }, function (err, StaffFinds) {
        if (StaffFinds.length < 1) {
            res.status(404).json({
                pesan: "user tidak ditemukan"
            })
        } else {
            let id_device = req.params.id_device;
            data.id_building = req.params.building_name;
            let id_type =  req.params.type;
            data.id_type = id_type;
            Repair.find({ id_device: id_device, id_type : id_type, id_building : data.id_building })
                .populate(["id_staff"])
                .exec()
                .then(allData => {
                    if (allData.length < 1) {
                        res.status(200).json({
                            status: 0,
                            created_at: 0,
                            created_by: 0,
                            name : 0
                        })
                    } else {
                        let size = allData.length - 1;
                        res.status(200).json({
                            status: allData[size].detail.status_repair,
                            data: allData[size].detail.data_repair,
                            created_at: allData[size].detail.created_at,
                            created_by: allData[size].id_staff.nip,
                            name: allData[size].id_staff.detail[0].name
                        })

                    }
                })
        }
    
    });
}

exports.setRepair = setRepair;
exports.changeStatusRepair = changeStatusRepair;
exports.cekRepairNow = cekRepairNow;
