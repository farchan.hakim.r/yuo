const include = require('./routes/includeRouter/include');

const express = include.express;
const cors = include.cors;

const app = express();
app.use(cors())
app.disable('etag');

const logger = include.logger;
const path = include.path;
const bodyParser = include.bodyParser;

const user = require('./routes/users');
const pump = require('./routes/pumpRouter');
const mcfa = require('./routes/mcfaRouter');
const repair = require('./routes/repairRouter');
const kitchen = require('./routes/kitchenRouter');
const Environment = require('./routes/EnvironemtRouter');
// const sensor = require('./routes/sensorRouter');



app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// app.use('/sensor', sensor);
app.use('/user', user);
app.use('/mcfa', mcfa);
app.use('/pump', pump);
app.use('/blood_pressure', kitchen);
app.use('/step', Environment);
app.use('/repair', repair);


module.exports = app;