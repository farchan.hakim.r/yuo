var express = require('express');
var router = express.Router();

const EnvironmentController = require('./../controller/EnvironmentController');
const auth = require('./../middleware/authentication')

//BUAT POST DATA
router.post('/:id_jamaah', EnvironmentController.saveEnvironment, function(req, res, next) {});

//GET ALL DATA
router.get('/:id_jamaah', EnvironmentController.getEnvironment, function(req, res, next) {});

module.exports = router;