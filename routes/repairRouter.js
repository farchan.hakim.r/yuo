var express = require('express');
var router = express.Router();

const repairController = require('../controller/historyRepairController');
const auth = require('./../middleware/authentication')

//buat kirim parameter
router.post('/set/:building_name/:type/:id_device', repairController.setRepair, function(req, res, next) {});

router.post('/statusChange/:building_name/:type/:id_device', repairController.changeStatusRepair, function(req, res, next) {});


router.get('/cekStatus/:building_name/:type/:id_device', repairController.cekRepairNow, function(req, res, next) {});

module.exports = router;