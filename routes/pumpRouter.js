var express = require('express');
var router = express.Router();

const pumpController = require('./../controller/pumpController');
const pumpSensorController = require('./../controller/pumpSensorController');
const auth = require('./../middleware/authentication')
    /* GET users listing. */
    // router.get('/', auth.verifyToken, function(req, res) {
    //   res.send('respond with a resource');

// });


//buat kirim parameter
router.get('/', function(req, res, next) {
    res.send('dd')
});


//buat kirim parameter
router.post('/:building_name/:pump_type', auth.verifyToken, pumpController.savePump, function(req, res, next) {});

//buat kirim data sensor
router.post('/sensor/:building_name/:pump_type', pumpSensorController.savePumpSensor, function(req, res, next) {});

//buat get parameter sensor
router.get('/:building_name/:pump_type', auth.verifyToken, pumpController.getPump, function(req, res, next) {});
//buat get data sensor raw
router.get('/sensor/:building_name/:pump_type/:start_date/:end_date', auth.verifyToken, pumpSensorController.getPumpSensor, function(req, res, next) {});


router.post('/statusSensor/:building_name/:pump_type', pumpSensorController.setStatusSensor, function(req, res, next) {});


router.get('/statusSensor/:building_name/:pump_type', pumpSensorController.getPumpSensorStatus, function(req, res, next) {});


//get data per sensor
router.get('/app/:building_name/:pump_type/:index', auth.verifyToken, pumpSensorController.getDataBySensor, function(req, res, next) {});
router.get('/appAll/:building_name/:pump_type', pumpSensorController.getAllDataSensor, function(req, res, next) {});
router.get('/appGraph/:building_name/:pump_type/:size_data', auth.verifyToken, pumpSensorController.getGraphDataSensor, function(req, res, next) {});

//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlblN0YWZmIjp7Im5hbWUiOiJncmFoYXRlcmEiLCJwYXNzd29yZCI6IiQyYSQxMiRaYVNqL296MWNGOWZnYlU3MDlOREdlYlBoNGRRdGNBL01peFJXMXBMdFVyY0poSEljQXNTUyJ9LCJpYXQiOjE1NTM2NjU1Mjd9.H4ZBYpQJnpChK4RzuIOBbcJgnVJQbuN5iEfH71DLJgM
module.exports = router;

// headers: {
//     'Authorization' : 'bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlblN0YWZmIjp7Im5hbWUiOiJncmFoYXRlcmEiLCJwYXNzd29yZCI6IiQyYSQxMiRaYVNqL296MWNGOWZnYlU3MDlOREdlYlBoNGRRdGNBL01peFJXMXBMdFVyY0poSEljQXNTUyJ9LCJpYXQiOjE1NTM2NjU1Mjd9.H4ZBYpQJnpChK4RzuIOBbcJgnVJQbuN5iEfH71DLJgM',
// },