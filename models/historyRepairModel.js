const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const historyRepairSchema = new mongoose.Schema({
    id_staff :{ type: mongoose.Schema.Types.ObjectId, ref : 'Staff'},
    id_device : { type: String},
    id_type : {type : String},
    id_building : {type : String},
    detail : {
        _id : false,
        data_repair : [{type : String}],
        status_repair : {type : Number, default : 0},
        created_at : { type: String, default : moment().format() }, 
    }  
})

const HistoryRepair = mongoose.model('HistoryRepair', historyRepairSchema);

module.exports = HistoryRepair;
