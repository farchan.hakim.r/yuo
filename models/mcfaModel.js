const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const mcfaSchema = new mongoose.Schema({
      building_name : { type : String},
      mcfa_type : {type : String},
      detail : [{
            _id : false,
            data : [{type : Boolean}],
            created_at : { type: String, default : moment().format() },    
      }],
      detail_zone : [{type : String}],
      last_time_alarm :  { type: String, default : moment().format() }
})

const McfaSchema = mongoose.model('Mcfa', mcfaSchema);

module.exports = McfaSchema;
