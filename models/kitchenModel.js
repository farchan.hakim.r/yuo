const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const EnvironmentSchema = new mongoose.Schema({
    id_jamaah: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    detail_data: [{ // untuk data kirim
        _id: false,
        blood_pressure : {type : Number},
        updated_at: { type: String, default: moment().format() },
    }],



})

const Environment = mongoose.model('BloodPressure', EnvironmentSchema);

module.exports = Environment;