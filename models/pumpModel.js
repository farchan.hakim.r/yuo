const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const pumSchema = new mongoose.Schema({
    building_name: { type: String },
    pump_type: { type: String },
    detail: [{
        _id: false,
        data: [{ type: Number }],
        default: [{ type: Number }],
        created_at: { type: String, default: moment().format() },
    }]

})

const Pump = mongoose.model('Pump', pumSchema);

module.exports = Pump;