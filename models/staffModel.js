const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const staffSchema = new mongoose.Schema({
   
    id : {type : String, unique : true, required : true},
    detail : [{
        name : { type : String},
        pass : { type : String},
        created_at : { type: String, default : moment().format() }, 
    }],
})

const Staff = mongoose.model('User', staffSchema);

module.exports = Staff;
